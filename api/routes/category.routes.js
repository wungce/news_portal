const express = require('express')
const router = express.Router()
const CategoryController = require('../app/controllers/category.controller')
const category_ctr = new CategoryController()

const loginCheck = require("../app/middleware/auth.middleware")
const isAdmin = require("../app/middleware/abac.middleware")
const uploader = require("../app/middleware/uploader.middleware")

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/health'    
    next()  
}

router.route('/')
.get(category_ctr.categoryList)
.post(loginCheck, isAdmin, setDest, uploader.single("image"), category_ctr.addCategory)

router.get('/detail/:slug', category_ctr.getCategoryBySlug)



router.route('/:id')
.get(category_ctr.getById)
.put(loginCheck, isAdmin, setDest, uploader.single("image"), category_ctr.updateById)
.delete(loginCheck, isAdmin, category_ctr.deleteById)

module.exports = router