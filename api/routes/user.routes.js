const router = require('express').Router()
const UserController = require('../app/controllers/user.controller')
const user_ctr = new UserController()
const loginCheck = require('../app/middleware/auth.middleware')
const is_admin = require('../app/middleware/abac.middleware')
router.route('/')
.get(user_ctr.userList)

router.route('/:id')
.get(loginCheck, is_admin, user_ctr.userGetById)

module.exports = router