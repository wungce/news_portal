const express = require('express')
const router = express.Router()
const PostController = require('../app/controllers/post.controller')
const post_ctr = new PostController()

const loginCheck = require("../app/middleware/auth.middleware")
const isAdmin = require("../app/middleware/abac.middleware")
const uploader = require("../app/middleware/uploader.middleware")

const setDest = (req, res, next) => {
    req.dir = 'public/uploads/posts'    
    next()  
}

router.route('/')
.get(post_ctr.postList)
.post(loginCheck, isAdmin, setDest, uploader.single("image"), post_ctr.addPost)

router.get('/detail/:slug', post_ctr.getPostsBySlug)



router.route('/:id')
.get(post_ctr.getById)
.put(loginCheck, isAdmin, setDest, uploader.single("image"), post_ctr.updateById)
.delete(loginCheck, isAdmin, post_ctr.postDeleteById)

module.exports = router