const router = require('express').Router()
const AuthController = require('../app/controllers/auth.controller')
const auth_controller = new AuthController()

router
.post('/login', auth_controller.login)
.post('/register', auth_controller.register)

module.exports = router