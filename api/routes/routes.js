const express = require('express')
const app = express()

const user_routes = require('../routes/user.routes')
const auth_routes = require('./auth.routes')
const category_routes = require('./category.routes')
const post_routes = require('./post.routes')

//user auth
app.use(auth_routes)

   // users
app.use('/user', user_routes)

// category
app.use('/categories', category_routes)

app.use('/posts', post_routes)

module.exports = app