const CategoryService = require('../services/category.service')
const slugify = require("slugify")
class CategoryController {
    constructor() {
        this.category_srv = new CategoryService()
    }

    addCategory = async (req, res, next) => {
        try {
            const data = req.body
            console.log("data", data.title)

            // if (req.file) {
            //     data.image = req.file.filename
            // }
            this.category_srv.categoryValidation(req.body)
            let slug = slugify(data.title, { replacement: "-", lower: true })

            data.slug = await this.category_srv.generateUniqueSlug(slug)

            let news = await this.category_srv.createCategory(data)

            res.json({
                msg: "Category created.",
                status: true,
                result: news
            })
        } catch (error) {
            next(error)
        }
    }
    categoryList = async (req, res, next) => {
        try {
            let categories_list = await this.category_srv.allGetCategory()

            res.json({
                msg: "All category fetched.",
                status: true,
                result: categories_list
            })

        } catch (error) {
            next(error)
        }

    }
    getById = async (req, res, next) => {
        try {
            let id = req.params.id
            console.log(id)
            let data = await this.category_srv.getListById(id)
            res.json({
                msg: "Category fetched by Id.",
                status: true,
                result: data
            })
        } catch (error) {
            next(error)
        }
    }
    updateById = async (req, res, next) => {
        try {
            let data = req.body
            console.log("data", data)
            console.log(req.params.id)
            // if (req.file) {
            //     data.image = req.file.filename
            // } else {
            //     delete data.image
            // }

            this.category_srv.categoryValidation(data)
            this.category_srv.UpdateCategoryById(data, req.params.id)
            res.json({
                result: data,
                status: true,
                msg: "Category update successfully."
            })

        } catch (error) {
            next(error)
        }

    }
    deleteById = async (req, res, next) => {
        try {

            let id = req.params.id
            let data = await this.category_srv.categoryDeleteById(id)
            res.json({
                msg: "Delete Category.",
                status: true,
                result: data
            })
        } catch (error) {
            next(error)
        }
    }
    getCategoryBySlug = async (req, res, next) => {
        try {
            console.log(req)
            let data = req.params.slug
            console.log(data)
            console.log("Testing")

            let response = await this.category_srv.getPostsBySlug(req.params.slug)
            res.json({
                msg: "Category Detail from slug Fetch",
                status: true,
                result: response
            })
        } catch (error) {
            next(error)
        }
    }
}

module.exports = CategoryController
