const AuthService = require('../services/auth.service')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

class AuthController {
    constructor() {
        this.auth_srv = new AuthService()
    }
    login = async (req, res, next) => {
        try {
            let data = req.body
            let validation_msg = this.auth_srv.loginValidation(data)

            if (validation_msg) {
                next({
                    msg: validation_msg,
                    status: 422,
                })
            } else {
                let user = await this.auth_srv.login(data)
                let token = await this.auth_srv.getToken({
                    id : user._id,
                    name : user.name
                })
                res.json({
                    msg: "Login successfully.",
                    status: true,
                    result : {
                        user : user,
                        access_token : token
                    }
                })
            }
        } catch (error) {
            next(error)
        }
    }
    register = async (req, res, next) => {
        try{
            let data = req.body;
            this.auth_srv.registerValidation(data)
            data.password = bcrypt.hashSync(data.password, 10)
            let userModel = await this.auth_srv.registerUser(data)
            if(userModel){
                res.json({
                    msg : "Register user",
                    status : true,
                    result : userModel
                })
            }
        } catch(error){
            next({
                status :422,
                msg : error
            })
        }
       

    }
}

module.exports = AuthController