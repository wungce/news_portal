const PostService = require('../services/post.service')
const slugify = require("slugify")
class PostController {
    constructor() {
        this.post_srv = new PostService()
    }

    addPost = async (req, res, next) => {
        try {
            const data = req.body
            console.log("data", data.title)


            if (req.file) {
                data.image = req.file.filename
            }
            this.post_srv.postValidation(req.body)
            let slug = slugify(data.title, { replacement: "-", lower: true })

            data.slug = await this.post_srv.generateUniqueSlug(slug)

            let news = await this.post_srv.createPost(data)

            res.json({
                msg: "Post created.",
                status: true,
                result: news
            })
        } catch (error) {
            next(error)
        }
    }
    postList = async (req, res, next) => {
        try {
            let post_list = await this.post_srv.allGetPosts()

            res.json({
                msg: "All post fetched.",
                status: true,
                result: post_list
            })

        } catch (error) {
            next(error)
        }

    }
    getById = async (req, res, next) => {
        try {
            let id = req.params.id
            console.log(id)
            let data = await this.post_srv.getListById(id)
            res.json({
                msg: "Post fetched by Id.",
                status: true,
                result: data
            })
        } catch (error) {
            next(error)
        }
    }
    updateById = async (req, res, next) => {
        try {
            let data = req.body
            console.log("data", data)
            console.log(req.params.id)
            if (req.file) {
                data.image = req.file.filename
            } else {
                delete data.image
            }

            this.post_srv.postValidation(data)
            this.post_srv.UpdateCategoryById(data, req.params.id)
            res.json({
                result: data,
                status: true,
                msg: "Post update successfully."
            })

        } catch (error) {
            next(error)
        }
    }
    postDeleteById = async (req, res, next) => {
        try {

            let id = req.params.id
            let data = await this.post_srv.postDeleteById(id)
            res.json({
                msg: "Delete posts.",
                status: true,
                result: data
            })
        } catch (error) {
            next(error)
        }
    }
    getPostsBySlug = async (req, res, next) => {
        try {
            let data = req.params.slug
            console.log(data)
            console.log("Testing")

            let response = await this.post_srv.getSlug(req.params.slug)
            let cats = response.category
            let related_posts = await this.post_srv.getPostByCatIds(cats)
            console.log(related_posts)
            res.json({
                msg: "Post Detail from slug Fetch",
                status: true,
                result: {response, related_posts}

            })
        } catch (error) {
            next(error)
        }
    }
}

module.exports = PostController
