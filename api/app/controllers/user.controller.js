const AuthServive = require("../services/auth.service")
class UserController {
    constructor() {
        this.auth_srv = new AuthServive()
    }
    userList = async (req, res, next) => {
        try {
            let user = await this.auth_srv.getAllUser()
            if(user) {
                res.json({
                    msg: "All user fetch",
                    status: true,
                    result: user,
                })
            }else{
                throw {
                    msg : "user don not fetch",
                }
            }
        } catch (error) {
            next(error)
        }

    }
    userGetById = async(req, res, next) => {
       try{
        let id = req.params.id
        let response = await this.auth_srv.getUserById(id)
        res.json({
            msg : "User fetched from id.",
            status : true,
            msg : response
        })
       } catch(error){
        next(error)
       }
    }
}

module.exports = UserController