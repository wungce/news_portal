const userModel = require('../models/user.model')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const CONFIG = require('../config/config')

class AuthService {
    loginValidation = (data) => {
        let msg = null
        if (!data.email || !data.password) {
            msg = "Credential are required."
        } else {
            msg = null
        }

        return msg;
    }
    registerValidation = (data) => {
        let msg = {}
        if(!data.name){
            msg['name'] = "Name is required"
        }
        if(!data.email){
            msg['email'] = "Email is required"
        }
        if(!data.password){
            msg['password'] = "Password is required"
        }
        if(!data.role){
            msg['role'] = "Role is required"
        }else{
            if(data.role !== 'admin' && data.role !== 'user'){
                msg['role'] = "User default set."
            }
        }
       if(Object.keys(msg).length > 0){
        throw msg
       }else{
        return null
       }
    }
    registerUser = (data) => {
        let user = new userModel(data)
        return user.save()
    }
    getAllUser = async () => {
        try{
            let user = await userModel.find()
            return user

        } catch(error){
            throw error
        }
    }
    login = async (data) => {
        try{
            let user = await userModel.findOne({
                email : data.email
            })
            if(user){
                //comparing two password from form data and data base..
                if(bcrypt.compareSync(data.password, user.password)){
                    return user
                }else{
                    throw {
                        msg : "Credentials does not match."
                    }
                }
            }else{
                throw {
                    msg : "User does not exist.",
                    status : 400
                }
            }
        } catch(error){
            throw error
        }
    }
    getToken = async(data) => {
        let token = jwt.sign(data, CONFIG.JWT_SECRET)
        return token
    }
    getUserById = async(id) => {
        try{
            let ack = await userModel.findById(id)
            if(ack){
                return ack
            }else{
                throw{
                    msg : "User dose not have database",
                    status : 422
                }
            }
        } catch(error){
            throw error
        }
    }
}
module.exports = AuthService