const CategoryModel = require('../models/category.model')

class CategoryService {
    categoryValidation = (data) => {
        let msg = {}
        if (!data.title) {
            msg['title'] = "Title is required."
        }
        if (!data.status) {
            msg['status'] = "Status is required."
        }
        if (Object.keys(msg).length > 0) {
            throw {
                msg: msg,
                status: 400
            }
        } else {

            return msg
        }
    }
    createCategory = async (data) => {
        try {
            let category = new CategoryModel(data)
            let ack = await category.save()
            if (ack) {
                return category
            } else {
                throw {
                    status: 400,
                    msg: "category create error"
                }
            }

        } catch (error) {
            throw error
        }
    }
    allGetCategory = async () => {
        try {
            let ack = await CategoryModel.find()
            if (!ack.length > 0) {
                throw {
                    msg: "News list is empty.",
                    status: 422
                }
            } else {
                return ack
            }
        } catch (error) {
            throw error
        }
    }
    getListById = async (id) => {
    //   return  await CategoryModel.findById(id)
    try {
        let ack = await CategoryModel.findById(id)
        if (ack) {
            return ack
        } else {
            throw {
                msg: "Category dose not fetch through getById.",
                status: 422
            }
        }
    } catch (error) {
        throw error
    }
    }
    UpdateCategoryById = async (data, id) => {
        try {
            let ack = await CategoryModel.findByIdAndUpdate(id, {
                $set: data
            })

            if (ack) {
                return ack
            } else {
                throw {
                    msg: "Sport does not update",
                    status: 400,
                }
            }
        } catch (error) {
            throw error
        }
    }
    categoryDeleteById = async (id) => {
        try {
            let ack = await CategoryModel.findByIdAndDelete(id)
            if (ack) {
                return ack
            } else {
                throw {
                    msg: "Category dose not delete.",
                    status: 422
                }
            }
        } catch (error) {
            throw error
        }
    }
    getPostsBySlug = async(slug) => {
        try{
            console.log(slug)
            return await CategoryModel.findOne({slug:slug})
            .populate("title")
            .populate("status")
        } catch(error){
            throw error
        }
    }
    generateUniqueSlug = async (slug) => {
        let exists = await CategoryModel.findOne({
            slug: slug
        })
        if (exists){
            slug = slug+Date.now()
            await this.generateUniqueSlug(slug)
        } else {
            return slug
        }
    }
}

module.exports = CategoryService