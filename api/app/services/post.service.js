const PostModel = require('../models/post.model')

class PostService {
    postValidation = (data) => {
        let msg = {}
        if (!data.title) {
            msg['title'] = "Title is required."
        }
        if (!data.author) {
            msg['author'] = "Author is required."
        }
        if (!data.description) {
            msg['description'] = "Description is required."
        }
        if (!data.status) {
            msg['status'] = "Status is required."
        }
        if (Object.keys(msg).length > 0) {
            throw {
                msg: msg,
                status: 400
            }
        } else {

            return msg
        }
    }
    createPost = async (data) => {
        try {
            let post = new PostModel(data)
            let ack = await post.save()
            if (ack) {
                return post
            } else {
                throw {
                    status: 400,
                    msg: "Post create error"
                }
            }

        } catch (error) {
            throw error
        }
    }
    allGetPosts = async () => {
        try {
            let ack = await PostModel.find({}).populate("category")
            if (!ack.length > 0) {
                throw {
                    msg: "Posts list is empty.",
                    status: 422
                }
            } else {
                return ack
            }
        } catch (error) {
            throw error
        }
    }
    getListById = async (id) => {
    //   return  await PostModel.findById(id)
    try {
        let ack = await PostModel.findById(id)
        if (ack) {
            return ack
        } else {
            throw {
                msg: "Post dose not fetch through getById.",
                status: 422
            }
        }
    } catch (error) {
        throw error
    }
    }
    getPostByCatIds = async (cat_id) => {
        return await PostModel.find({
          category : {$in: cat_id}
        })
        .populate("title")
        .populate("status")
      }
    UpdateCategoryById = async (data, id) => {
        try {
            let ack = await PostModel.findByIdAndUpdate(id, {
                $set: data
            })

            if (ack) {
                return ack
            } else {
                throw {
                    msg: "Sport does not update",
                    status: 400,
                }
            }
        } catch (error) {
            throw error
        }
    }
    postDeleteById = async (id) => {
        try {
            let ack = await PostModel.findByIdAndDelete(id)
            if (ack) {
                return ack
            } else {
                throw {
                    msg: "Category dose not delete.",
                    status: 422
                }
            }
        } catch (error) {
            throw error
        }
    }
    getSlug = async(slug) => {
        try{
            return await PostModel.findOne({slug:slug})
            .populate("title")
            .populate("author")
            .populate("description")
            .populate("image")
            .populate("status")
        } catch(error){
            throw error
        }
    }
    generateUniqueSlug = async (slug) => {
        let exists = await PostModel.findOne({
            slug: slug
        })
        if (exists){
            slug = slug+Date.now()
            await this.generateUniqueSlug(slug)
        } else {
            return slug
        }
    }
}

module.exports = PostService