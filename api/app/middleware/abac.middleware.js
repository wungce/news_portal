const isAdmin = (req, res, next) => {
    let user = req.auth_user
    let role = user.role
    if(role.includes('admin')){
        next()
    }else{
        next({
            status : 403,
            msg : "You do not have permission."
        })
    }
}

module.exports = isAdmin