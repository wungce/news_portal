const mongoose = require('mongoose')
const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["active", "inactive"],
        required: true
    },
    slug : {
        type : String,
        required : true,
        unique : true,
    },
    category: {
        type :  mongoose.Types.ObjectId,
        ref: "Category",
        required : true
    },
    image: {
        type: String,
    },
}, {
    timestamps: true,
    autoCreate: true,
    autoIndex: true
})


module.exports = mongoose.model("Post", PostSchema)