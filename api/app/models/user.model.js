const mongoose = require('mongoose')
// mongoose.set("strictQuery", false);

const UserSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
    },
    role : {
        type : String,
        enum : ['admin', 'user']
    }
}, {
    timestamps : true,
    autoIndex : true,
    autoCreate : true,
})

module.exports = mongoose.model("User", UserSchema)