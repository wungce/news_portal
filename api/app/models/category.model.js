const mongoose = require('mongoose')
const CategorySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["active", "inactive"],
        required: true
    },
    slug : {
        type : String,
        required : true,
        unique : true,
    },
}, {
    timestamps: true,
    autoCreate: true,
    autoIndex: true
})


module.exports = mongoose.model("Category", CategorySchema)