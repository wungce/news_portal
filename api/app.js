const express = require('express')
const app = express()
const routes = require('./routes/routes')
require('./app/services/mongoose.service')

const cors = require('cors')
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use("/images", express.static(process.cwd()+"/public/uploads"))


app.use('/api/v1', routes)

app.use((req, res, next) => {
    next({
        msg : 'Not found',
        status : 404
    })
})

app.use((error, req, res, next) => {
    let status_code = error.status || 500
    let msg = error.msg || error

    res.status(status_code).json({
        msg : msg,
        result : null,
        status : false
    })
})
app.listen(3005, 'localhost', (err) => {
    if(err){
        console.log("Error does not listen")
    }else{
        console.log("This is port listening.")
    }

})